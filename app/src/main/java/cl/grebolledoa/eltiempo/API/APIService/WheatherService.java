package cl.grebolledoa.eltiempo.API.APIService;

import cl.grebolledoa.eltiempo.City;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WheatherService {

    @GET("weather")
    Call<City> getCity(@Query("q") String city, @Query("APPID") String apikey);

    @GET("weather")
    Call<City> getCity(@Query("q") String city, @Query("APPID") String apikey,
                       @Query("units") String units);

}
