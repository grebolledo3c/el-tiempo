package cl.grebolledoa.eltiempo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cl.grebolledoa.eltiempo.API.APIService.WheatherService;
import cl.grebolledoa.eltiempo.API.Api;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.preferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);

        if(this.preferences.getBoolean("isLogin", false) == true){
            Toast.makeText(this, "Hola " +
                    this.preferences.getString("username", ""),
                    Toast.LENGTH_LONG).show();
            callApi();
        }else{
            Toast.makeText(this, "Usted no está logeado", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        //readJsonObject();

        //readGsonObject();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        //vamos a cargar el menu que defininos
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        switch(menuItem.getItemId()){
            case R.id.menu_logout:
                logout();
                return true;
            case R.id.menu_usuario:
                String username = this.preferences.getString("username", "");
                Toast.makeText(this, "Hola "+username, Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private void logout(){
        //limpiar las preferencias de usuario
        this.preferences.edit().clear().apply();

        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void readJsonObject(){
        String json = "{"+
                            "id: 1,"+
                            "name: Santiago," +
                            "year_fundation: 1600"+
                       "}";

        try{
            JSONObject mJson = new JSONObject(json);
            int id = mJson.getInt("id");
            String name = mJson.getString("name");
            int yearFundation = mJson.getInt("year_fundation");

            Toast.makeText(this, id+ "     " + name + "     "+yearFundation, Toast.LENGTH_LONG).show();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void readGsonObject(){
        String json = "{"+
                "id: 1,"+
                "name: Santiago" +
                "}";

        Gson gson = new GsonBuilder().create();

        City city = gson.fromJson(json, City.class);

        Toast.makeText(this, city.getId()+"     "+city.getName(),
                Toast.LENGTH_LONG).show();

    }

    public void callApi(){
        WheatherService service = Api.getApi().create(WheatherService.class);

        //seteo los parametros al llamar al objeto getCity
        Call<City> cityCall = service.getCity("Santiago",
                "3d210272849ae814976d047ffd718f0f", "metric");

        //encolamos la llamada al servicio
        cityCall.enqueue(new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                City city = response.body();
                Log.i("response", response.body().toString());
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}