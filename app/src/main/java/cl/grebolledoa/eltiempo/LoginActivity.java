package cl.grebolledoa.eltiempo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private Button btLogin;
    private EditText etUsername, etPassword;
    private SharedPreferences preferences;


    private final static String USERNAME = "admin";
    private final static String PASSWORD = "admin..";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.btLogin = findViewById(R.id.bt_login);
        this.etUsername = findViewById(R.id.et_username);
        this.etPassword = findViewById(R.id.et_password);

        this.preferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);

        this.btLogin.setOnClickListener(v -> {
            String username = etUsername.getText().toString();
            String password = etPassword.getText().toString();
            if(username.equals(USERNAME) && password.equals(PASSWORD)){
                SharedPreferences.Editor editor = this.preferences.edit();
                editor.putBoolean("isLogin", true);
                editor.putString("username", username);
                editor.apply();
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }else{
                Toast.makeText(this,"Credenciales invalidas", Toast.LENGTH_LONG).show();
            }
        });

    }
}