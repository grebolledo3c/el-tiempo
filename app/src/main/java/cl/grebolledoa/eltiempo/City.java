package cl.grebolledoa.eltiempo;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class City {
    private int id;
    private String name;
    @SerializedName("main")
    private Temperature temperature;

    public City() {
    }

    public City(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Temperature parseJson(String response){
        Gson gson = new GsonBuilder().create();
        Log.i("parseJson", "1");
        return gson.fromJson(response, Temperature.class);
    }
}
